# Credit card fraud detection

Data source: https://www.kaggle.com/mlg-ulb/creditcardfraud

# Project goal

There are multiple tasks associated with the given data ranging from EDA, through feature engineering, to building and validating a predictive model while dealing with highly imbalanced data.

# Files

- *scripts* directory contains jupyter notebooks,
- *data*,
- *requirements.txt* - anaconda environment creation file.
